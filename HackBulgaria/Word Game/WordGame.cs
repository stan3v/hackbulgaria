﻿namespace Word_Game
{
    using System;
    using System.Linq;
    using System.Threading;

    class WordGame
    {
        public static void CenterMsg(string str)
        {
            Console.SetCursorPosition((Console.WindowWidth - str.Length) / 2, Console.CursorTop);
            Console.WriteLine(str);
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        static void Main(string[] args)
        {
            string gameName = "Word Game";
            string textIntroOne = "To start the game you should enter the size of a table";
            string textIntroTwo = "e.g. 3 4 for a table with 3 rows and 4 colums.";
            string textIntroThree = "Then press Enter.\n";
            string errorMsg = "Upss... Try Again!";
            string moveForwardMsg = "Well Done. We can now move forward.";
            string hintFirst = "Now is time to fill the table.";
            string hintSecond = "On the following lines enter as many letters as there are columns in it. (no spaces)";

            Console.ForegroundColor = ConsoleColor.White;
            CenterMsg(gameName);
            Console.ForegroundColor = ConsoleColor.Gray;

            CenterMsg(textIntroOne);

            Console.SetCursorPosition((Console.WindowWidth - textIntroTwo.Length) / 2, Console.CursorTop);
            Console.Write("e.g. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("3 4 ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("for table with 3 rows and 4 colums.");

            CenterMsg(textIntroThree);

            int rows;
            int columns;
            while (true)
            {
                try
                {
                    Console.SetCursorPosition(Console.WindowWidth / 2 - 2, Console.CursorTop);
                    Console.ForegroundColor = ConsoleColor.Green;

                    int[] tableSize = Console.ReadLine().Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(n => Convert.ToInt32(n)).ToArray();

                    if (tableSize.Length != 2)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    Console.WriteLine();

                    rows = tableSize[0];
                    columns = tableSize[1];

                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    string tableInfo = string.Format("You created a table with {0} rows and {1} colums.\n", rows, columns);

                    CenterMsg(moveForwardMsg);
                    CenterMsg(tableInfo);
                    break;
                }
                catch
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    CenterMsg(errorMsg);
                    Console.WriteLine();
                };
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            CenterMsg(hintFirst);
            CenterMsg(hintSecond);
            Console.WriteLine();

            char[,] myTabble = new char[rows, columns];

            string line = string.Empty;

            for (int i = 0; i < rows; i++)
            {
                while (true)
                {
                    Console.SetCursorPosition(Console.WindowWidth / 2 - (columns / 2), Console.CursorTop);
                    line = Console.ReadLine();
                    if (line.Length == columns)
                    {
                        break;
                    }
                }
                for (int j = 0; j < columns; j++)
                {
                    myTabble[i, j] = line[j];
                }
            }

            //И сега съществената част..

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string wordInput = "Good job! Now enter the word we're searching.";
            CenterMsg(wordInput);
            Console.WriteLine();
            Console.SetCursorPosition(Console.WindowWidth / 2 - (columns / 3), Console.CursorTop);
            Console.ForegroundColor = ConsoleColor.Red;

            string word = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine();
            CenterMsg("Ok, let's see...");
            Thread.Sleep(1500);

            int result = 0;

            //checking horizontaly 
            for (int row = 0; row < rows; row++)
            {
                string currentLine = string.Empty;
                for (int col = 0; col < columns; col++)
                {
                    currentLine += myTabble[row, col];
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            //checking vertically
            for (int col = 0; col < columns; col++)
            {
                string currentLine = string.Empty;
                for (int row = 0; row < rows; row++)
                {
                    currentLine += myTabble[row, col];
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            //checking across from left to right
            for (int row = rows - 1; row >= 0; row--)
            {
                string currentLine = string.Empty;
                int currentRow = row;
                for (int col = 0; col < columns; col++)
                {
                    if (currentRow != rows - 1)
                    {
                        currentLine += myTabble[currentRow, col];
                        currentRow++;
                    }
                    else
                    {
                        currentLine += myTabble[currentRow, col];
                        break;
                    }
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            for (int col = 1; col < columns; col++)
            {
                int currentCol = col;
                string currentLine = string.Empty;
                for (int row = 0; row < rows; row++)
                {

                    if (currentCol != columns - 1)
                    {
                        currentLine += myTabble[row, currentCol];
                        currentCol++;
                    }
                    else
                    {
                        currentLine += myTabble[row, currentCol];
                        break;
                    }
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            //checking across from right to left
            for (int row = rows - 1; row >= 0; row--)
            {
                string currentLine = string.Empty;
                int currentRow = row;
                for (int col = columns - 1; col >= 0; col--)
                {
                    if (currentRow != rows - 1)
                    {
                        currentLine += myTabble[currentRow, col];
                        currentRow++;
                    }
                    else
                    {
                        currentLine += myTabble[currentRow, col];
                        break;
                    }
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            for (int col = columns - 2; col >= 0; col--)
            {
                int currentCol = col;
                string currentLine = string.Empty;
                for (int row = 0; row < rows; row++)
                {

                    if (currentCol != 0)
                    {
                        currentLine += myTabble[row, currentCol];
                        currentCol--;
                    }
                    else
                    {
                        currentLine += myTabble[row, currentCol];
                        break;
                    }
                }

                if (currentLine.Contains(word))
                {
                    result += 1;
                }

                if (ReverseString(currentLine).Contains(word))
                {
                    result += 1;
                }
            }

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            CenterMsg(string.Format("Well done! Your table contains searching word {0} times!", result.ToString()));
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine();
        }
    }
}