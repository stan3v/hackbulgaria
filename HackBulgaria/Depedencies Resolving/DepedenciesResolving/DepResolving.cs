﻿namespace DepedenciesResolving
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    class DepResolving
    {
        static string installedModulesPath = ("..\\..\\..\\installed_modules");
        static string instalationInfo = string.Empty;

        public static void InstallPackage(string packageName, List<string> allPackages, List<string> dependencies)
        {
            if (Directory.Exists(installedModulesPath + "\\" + packageName))
            {

                if(!instalationInfo.Substring(Math.Max(0, instalationInfo.Length - 30)).Contains("Installing") && instalationInfo.Length>0)
                {
                instalationInfo = instalationInfo.Remove(instalationInfo.Length - 1);
                }

                instalationInfo +=string.Format(packageName.Remove(1).ToUpper() + string.Format("{0} is already installed.", packageName).Substring(1));
            }
            else
            {
                instalationInfo += string.Format("Installing {0}.\n", packageName);
                int k = 0;
                for (int j = 1; j < allPackages.Count - 1; j++)
                {
                    string searchingPackage = allPackages[j].Substring(3, allPackages[j].IndexOf(':') - 4);
                    if (packageName == searchingPackage)
                    {
                        k = j;
                        break;
                    }
                }
                string neededDependenciesString = allPackages[k].Substring(allPackages[k].IndexOf('[') + 1, allPackages[k].IndexOf(']') - allPackages[k].IndexOf('[') - 1);
                var values = neededDependenciesString.Split(',').Select(s => s.Replace("\"", "").Trim()).ToList();
                if (values[0] == "")
                {
                    Directory.CreateDirectory(installedModulesPath + "\\" + packageName);
                }
                else
                {
                    instalationInfo += string.Format("In ordred to install {0}, we need ", packageName);
                    for (int i = 0; i < values.Count; i++)
                    {
                        if (values.Count == 1)
                        {
                            instalationInfo += string.Format("{0}. \n", values[i].ToString());
                        }
                        else if (i != values.Count - 1)
                        {
                            instalationInfo += string.Format("{0}", values[i].ToString());
                        }
                        else
                        {
                            instalationInfo += string.Format(" and {0}. \n", values[i].ToString());
                        }
                    }
                    foreach (var item in values)
                    {
                        InstallPackage(item, allPackages, dependencies);
                        Directory.CreateDirectory(installedModulesPath + "\\" + packageName);
                    }

                }
            }
        }

        static void Main(string[] args)
        {
            List<string> dependencies = new List<string>();
            using (var sr = new StreamReader("..\\..\\..\\dependencies.json"))
            {
                while (sr.Peek() >= 0)
                    dependencies.Add(sr.ReadLine());
            }

            List<string> allPackages = new List<string>();
            using (var sr = new StreamReader("..\\..\\..\\all_packages.json"))
            {
                while (sr.Peek() >= 0)
                    allPackages.Add(sr.ReadLine());
            }

            for (int i = 2; i < dependencies.Count - 1; i++)
            {
                string currentDependence = dependencies[i].Substring(dependencies[i].IndexOf('[') + 2, dependencies[i].IndexOf(']') - dependencies[i].IndexOf('[') - 3);
                InstallPackage(currentDependence, allPackages, dependencies);
            }
            if (instalationInfo.Substring(Math.Max(0, instalationInfo.Length - 1)).Contains('\n'))
            {
                instalationInfo = instalationInfo + "All done.\n";
            }
            else
            {
                instalationInfo = instalationInfo + "\nAll done.\n";
            }
            System.Diagnostics.Process.Start("..\\..\\..\\installed_modules");
            Console.WriteLine(instalationInfo);
        }
    }
}