﻿namespace Points
{
    using System;
    using System.Numerics;

    class Points
    {
        static void Main(string[] args)
        {
            int warpSymbol = 1;

            string inputPoint = Console.ReadLine();

            BigInteger xPoint = BigInteger.Parse(inputPoint.Substring(inputPoint.IndexOf('(') + 1, (inputPoint.IndexOf(',') - inputPoint.IndexOf('(') - 1)));
            BigInteger yPoint = BigInteger.Parse(inputPoint.Substring(inputPoint.IndexOf(',') + 2, (inputPoint.IndexOf(')') - inputPoint.IndexOf(',') - 2)));

            string inputDirection = Console.ReadLine();

            for (int i = 0; i <= inputDirection.Length - 1; i++)
            {
                if (inputDirection[i] == '~')
                {
                    warpSymbol *= -1;
                }

                else if (inputDirection[i] == '>')
                {
                    xPoint += 1 * warpSymbol;
                }

                else if (inputDirection[i] == '<')
                {
                    xPoint -= 1 * warpSymbol;
                }

                else if (inputDirection[i] == '^')
                {
                    yPoint -= 1 * warpSymbol;
                }

                else
                {
                    yPoint += 1 * warpSymbol;
                }
            }

            Console.WriteLine("({0}, {1})", xPoint, yPoint);
        }
    }
}